from random import randint

from server import start_and_wait

from identifier_pb2 import Identifier
from identifier_pb2_grpc import IdentifierGeneratorServicer, \
    add_IdentifierGeneratorServicer_to_server as adder

class IdentifierGenerator(IdentifierGeneratorServicer):
  def Generate(self, request, context):
    return Identifier(code=hex(randint(0x111111, 0xFFFFFF))[2:])

if __name__ == '__main__':
  start_and_wait(50052, IdentifierGenerator(), adder)

