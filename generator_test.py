import generators as g

from invoice import InvoiceProcessor
from invoice_test import identifierStub

def test_generated_invoice_is_valid():
  invoice = g.invoice()
  assert invoice.id == ''
  assert invoice.HasField('created')
  assert not invoice.HasField('processed')
  assert len(invoice.errors) == 0
  processor = InvoiceProcessor(identifierStub())
  result = processor.Process(invoice, None)
  assert len(result.errors) == 0
  assert result.HasField('created')
  assert result.HasField('processed')

def test_generated_invoice_skus_are_4_characters_long():
  invoice = g.invoice()
  assert all([len(i.sku) == 4 for i in invoice.items])

