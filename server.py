import asyncio
import functools
import grpc
import os
import signal

from concurrent import futures

def ask_exit(loop, server):
  print("Stopping server...")
  server.stop(0)
  loop.stop()

def start_and_wait(port, service, adder):
  print("Starting server at localhost:%i" % port)
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  adder(service, server)
  server.add_insecure_port('[::]:{0}'.format(port))
  server.start()

  loop = asyncio.get_event_loop()
  for signame in ('SIGINT', 'SIGTERM'):
    partial = functools.partial(ask_exit, loop, server)
    loop.add_signal_handler(getattr(signal, signame), partial)

  print("pid %s: send SIGINT or SIGTERM to exit (CTRL+C)" % os.getpid())
  try:
    loop.run_forever()
  finally:
    loop.close()

