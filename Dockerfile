FROM python:3.6.4
MAINTAINER "Luis Belloch <luis@luisbelloch.es>"

WORKDIR /opt/invoices

COPY *.py protos requirements.txt Makefile ./

RUN pip install -r requirements.txt && make

EXPOSE 50051 50052

